#!/usr/bin/env ruby
#get a table(ps_v[...] + run_results[...])
#sample of data.csv which includes ps_v(overlap is forgived) with run_results
#ParamA,ParamB,ResultX
#0,0,100.0
#0,0,110.0
#0,1,210.0
#0,1,210.0
#1,0,50.0
#1,0,60.0
#1,1,10.0
#1,1,20.0

require_relative "#{ENV['OACIS_ROOT']}/config/environment"
require_relative "../lib/aggregate_functions"
require_relative "../lib/csv_writer"
require 'csv'

#change the following values
result_key = "ResultX"
sim = Simulator.where(name: "my_simulator").first
#get all ps with finished runs
#target_ps_ids = sim.runs.where(status: :finished).map(&:parameter_set_id).uniq
#get a first ps with finished runs
target_ps_ids = [sim.runs.where(status: :finished).first.parameter_set.id]

ps_list={}
ps_aggregate( target_ps_ids ).each do |ps|
  ps_list[ps["_id"]]=ps["v"]
end
parameter_keys = ps_list[ps_list.keys.first].keys

run_list = {}
run_aggregate_with_result( target_ps_ids, result_key ).each do |run|
  run_list[run["_id"]]=ps_list[run["parameter_set_id"]]
  run_list[run["_id"]]["#{result_key}"]=run["result"]
end
table_cols = run_list.keys.map {|id| run_list[id].keys.map {|v_key| run_list[id][v_key]}}

headers = parameter_keys + ["#{result_key}"]
csv_write( "data.csv", headers, table_cols )

