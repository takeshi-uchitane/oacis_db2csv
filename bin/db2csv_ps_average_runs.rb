#!/usr/bin/env ruby
#get a table(ps_v[...] + average_of_run_results[...])
#sample of data.csv which includeis unique ps_v with average of run_results
#ParamA,ParamB,average,square_average,count
#0,0,105.0,2210.0,2
#0,1,205.0,84100.0,2
#1,0,55.0,6100.0,2
#1,1,15.0,500.0,2

require_relative "ENV['OACIS_ROOT']/config/environment"
require_relative "../lib/aggregate_functions"
require_relative "../lib/csv_writer"
require 'csv'

#change the following values
result_key = "ResultX"
sim = Simulator.where(name: "my_simulator").first
#get all ps with finished runs
#target_ps_ids = sim.runs.where(status: :finished).map(&:parameter_set_id).uniq
#get a first ps with finished runs
target_ps_ids = [sim.runs.where(status: :finished).first.parameter_set.id]

ps_list = {}
ps_aggregate( target_ps_ids ).each do |ps|
  ps_list[ps["_id"]]=ps["v"]
end
parameter_keys = ps_list[ps_list.keys.first].keys

run_aggregate_with_average( target_ps_ids, result_key ).each do |run|
  ps_id = run["_id"]
  ps_list[ps_id]["average"]=run["average"]
  ps_list[ps_id]["square_average"]=run["square_average"]
  ps_list[ps_id]["count"]=run["count"]
end
table_cols = ps_list.keys.map {|id| ps_list[id].keys.map {|v_key| ps_list[id][v_key]}}

headers = parameter_keys + ["average", "square_average", "count"]
csv_write( "data.csv", headers, table_cols )

