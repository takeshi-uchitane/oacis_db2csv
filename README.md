# Samples of oacis api to get csv file from DB

## Getting start
- step 0. [OAICS](https://github.com/crest-cassia/oacis) is requied.
    - A simulator named `my_simulator` which has parameter keys `ParamA` and `ParamB` is installed
    - A run of the simulator has result_key `"ResultX` in _output.json `{"ResultX": 100.0}`
- step 1. download samples

```bash
git clone https://takeshi-uchitane@bitbucket.org/takeshi-uchitane/oacis_db2csv.git
```

- step 2. set OACIS_ROOT

```bash
echo 'export OACIS_ROOT=/path/to/oacis_root_dir' >> ~/.bashrc
source ~/.bashrc
```

- step 3. change `result_key`, `sim` and `target_ps_ids` in each samples(./bin/db2csv_*.rb)

```ruby
result_key = "ResultX"
sim = Simulator.where(name: "my_simulator").first
target_ps_ids = sim.runs.where(status: :finished).map(&:parameter_set_id).uniq
```

- step 4. run the script

```bash
./bin/db2csv_*.rb
```

- step 5. you can get data.csv

```csv
ParamA,ParamB,ResultX
0,0,100.0
0,0,110.0
0,1,210.0
0,1,210.0
1,0,50.0
1,0,60.0
1,1,10.0
1,1,20.0
```

## Tips
- result value must be either of `Integer` or `Float`

# License

The MIT License (MIT)

Copyright (c) 2016 Takeshi Uchitane

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.