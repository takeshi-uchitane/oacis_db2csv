def csv_write( file_name, headers, data)

  CSV.open(file_name, "w") do |csv|
    csv << headers
    data.each do |elements|
      csv << elements
    end
  end
end
