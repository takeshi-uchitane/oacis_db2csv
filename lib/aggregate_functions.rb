def ps_aggregate( target_ps_ids)

  ParameterSet.collection.aggregate([
    { '$match' => ParameterSet.in(id: target_ps_ids).selector },
    { '$group' => { _id: '$_id', v: {'$first' => '$v'} }}
  ])
end

def run_aggregate_with_average( target_ps_ids, result_key )

  Run.collection.aggregate([
    { '$match' => Run.in(parameter_set_id: target_ps_ids).where(status: :finished).exists("result.#{result_key}" => true).selector },
    { '$project' => { parameter_set_id: 1, result_val: "$result.#{result_key}" }},
    { '$group' => { _id: '$parameter_set_id', average: {'$avg' => '$result_val'}, square_average: {'$avg' => {'$multiply' =>['$result_val', '$result_val']} }, count: {'$sum' => 1} }}
  ])
end

def run_aggregate_with_result( target_ps_ids, result_key )

  Run.collection.aggregate([
    { '$match' => Run.in(parameter_set_id: target_ps_ids).where(status: :finished).exists("result.#{result_key}" => true).selector },
    { '$project' => { parameter_set_id: 1, result_val: "$result.#{result_key}" }},
    { '$group' => { _id: '$_id', parameter_set_id: {'$first' =>'$parameter_set_id'}, "result": {'$first' => "$result_val"}} }
  ])
end
